<?php
session_start();
$message_count = 0;
$all_messages = array();
$msg_details = array();
$token = $_SESSION["access_token"];

getMessages();
                
getMessageDetails(); 

function getMessages(){
    global $message_count, $all_messages, $token;
    $authorization = "Authorization: Bearer ".$token;
    $pageToken = NULL;

    // get all e-mails
    do {
        try {
          if ($pageToken) {
            $opt_param['pageToken'] = $pageToken;
          }

          $curl = curl_init();
          curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
          curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($curl, CURLOPT_URL,"https://www.googleapis.com/gmail/v1/users/me/messages");
          curl_setopt($curl, CURLOPT_HTTPGET, true);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          $response = curl_exec($curl);

          curl_close($curl);
                    
          $message_list = json_decode($response, true);
          foreach ($message_list['messages'] as $msg) {
            $message_count++;
            array_push($all_messages, $msg['id']);               
          }

          if(isset($message_list['nextPageToken'])){
            $pageToken = $message_list['nextPageToken'];
          }

        } catch (Exception $e) {
          print 'An error occurred: ' . $e->getMessage();
        }
      } while ($pageToken);
}

function getMessageDetails(){
    global $all_messages,$msg_details,$token;
    $authorization = "Authorization: Bearer ".$token;
  
    // get message content
    foreach ($all_messages as $msg) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_URL,"https://www.googleapis.com/gmail/v1/users/me/messages/" . $msg . "?format=metadata&fields=payload");
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
    
        curl_close($curl);

        $response = json_decode($response, true);
        
        // get from mail address
        foreach ($response['payload']['headers'] as $msg_field) {
            if($msg_field['name'] == "From"){
                preg_match('/<(.*?)>/', $msg_field['value'], $match);
                array_push($msg_details, $match[1]);
            }
          }   
    }

    $frequencies = array_count_values($msg_details);
    arsort($frequencies);
    $tenFrequencies = array_slice($frequencies, 0, 10, TRUE);
    echo json_encode($tenFrequencies);

}

?>