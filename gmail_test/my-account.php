<?php
session_start();

if (isset($_POST['act'])) {
    if($_POST['act'] == 'log_out') {
        session_destroy();
        echo "<script>location.href='index.php';</script>";
    }
}

if(!isset($_SESSION["access_token"])){
    echo "<script>location.href='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $("#getTopSender").click(function(){
            $(this).attr("disabled",true);
            $.ajax({
            type : 'POST',
            url : 'services.php',
            data : "",
            cache: false,
            success : function(result){
                var response = $.parseJSON(result)
                var list ="";
                $.each (response, function (item) {
                    list +=  item + " : " + response[item] + " mail(s)<br>";
                });

                $("#result").append(list);
                $("#getTopSender").attr("disabled",false);
            }
            });
        });
    });
</script>
</head>
<body>
<form action="my-account.php" method="post">
        <button type="submit" name="act" value="log_out" >Sign Out</button>
</form>
<hr>
<form action="my-account.php"  method="post">
    <button type="button" name="act" value="get_top_sender" id="getTopSender">Get Top 10 Sender</button>
</form>
<hr>
<div id="result"></div>
</body>
</html>