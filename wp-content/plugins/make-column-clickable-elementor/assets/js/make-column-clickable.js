jQuery( function( $ ) {
  $( 'body:not(.elementor-editor-active) .make-column-clickable-elementor' ).click( function() {
    window.location = $( this ).data( 'column-clickable' );
    return false;
  });
});
