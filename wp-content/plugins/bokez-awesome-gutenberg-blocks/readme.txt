=== Bokez — Awesome Gutenberg Blocks ===
Requires at least: 4.5
Tested up to: 5.0.0
Requires PHP: 5.4
Stable tag: 1.7.1
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: posts grid block, gutenberg, gutenberg blocks, gutenberg editor, wordpress 5 editor, gutenberg addon, wordpress 5, customizable blocks

14 Essential Gutenberg Blocks. Customizable and super easy to use.

== Description ==

Bokez is an awesome collection of blocks which made for the new WordPress Gutenberg Editor. With Bokez you get your essential blocks, customization options and more. 

Download now and start creating beautiful looking content with ease and without typing a single line of code.

**14 BLOCKS :**

1. Video Popup Block : [Demo](http://demo.delabon.com/bokez/video-popup/)
2. Pricing Table Block : [Demo](http://demo.delabon.com/bokez/pricing-table)
3. Posts Grid Block : [Demo](http://demo.delabon.com/bokez/posts-grid)
4. Testimonial Block ( Multiple Testomonials ) : [Demo](http://demo.delabon.com/bokez/testimonial)
5. Accordion / FAQ Block ( Multiple Accordions ) : [Demo](http://demo.delabon.com/bokez/accordion-faq)
6. Profile Block : [Demo](http://demo.delabon.com/bokez/profile)
7. Cover Block : [Demo](http://demo.delabon.com/bokez/cover)
8. Share Icons Block : [Demo](http://demo.delabon.com/bokez/share)
9. Button Block : [Demo](http://demo.delabon.com/bokez/button)
10. Notification Block : [Demo](http://demo.delabon.com/bokez/notification)
11. Blockquote Block : [Demo](http://demo.delabon.com/bokez/blockquote/)
12. Divider Block : [Demo](http://demo.delabon.com/bokez/divider)
13. Progress Bar Block : [Demo](http://demo.delabon.com/bokez/progress-bar)
14. Separator With Heading Block : [Demo](http://demo.delabon.com/bokez/separator-with-text)

== More Powerful Plugins ==

* [Faceproof - Boost Woocommerce Sales Instantly](https://codecanyon.net/item/faceproof-boost-woocommerce-sales-instantly/22835201)

A Woocommerce plugin that displays your product recent customers next to the Add To Cart button. Powerful Woocommerce social proof method to increase conversion rate instantly.

* [Woomotiv - Woocommerce Social Proof Plugin](https://codecanyon.net/item/woomotiv-woocommerce-social-proof-notifications-instantly-increases-conversion-rates/22424874)

Boost your woocommerce sales using this social proof plugin. Woomotiv displays recent sales notifications to your visitors to make your site trustworthy.

* [Ely - Fancy Wordpress Gallery Plugin](https://codecanyon.net/item/ely-wordpress-gallery-plugin/22116166)

Create awesome, elegant and perfect galleries easily in your wordpress editor.

* [More Wordpress Plugins](https://delabon.com)

== Installation ==
**Gutenberg plugin** is required for Bokez to work. so please install **Gutenberg** and **Bokez**, and after that activate them.

== Screenshots ==

1. Pricing Table
2. Posts Grid
3. Accordion / FAQ
4. Testimonials
5. Cover / Header
6. Separator With Text
7. Posts Grid Editor
8. Accordion / FAQ Editor

== Changelog ==

= 1.7.1 =

* Code Refactor

= 1.7.0 ( Wordpress 5.0 Compatible ) =

* Unminified Source
* Improved: Code.
* Fixed: Bugs.

= 1.6.0 =

* New: Getting Started Guide.
* Improved: Testimonial Block.
* Improved: Notifications Block.
* Improved: Cover Block.

= 1.5.1 =

* Removed Duplicated Code.

= 1.5.0 =

* Added : Video Popup Block.
* Code Improvements.

= 1.4.4 =

* Fixed : Some Css Bugs
* Updated : Pricing Table block to work with the latest gutenberg api.
* Updated : Button block to work with the latest gutenberg api.
* Updated : Posts Grid block to work with the latest gutenberg api.

= 1.4.0 =

* Added : Pricing Table Block.
* Fixed : Notification Block Color Not Changing Bug.
* Fixed : Notification Block Margin Bug.
* Added : New Icon To Posts Grid Block.

= 1.3.0 =

* Improved : Testimonial Block ( Ability to add multiple testimonials in the same block ).
* Improved : Share Block ( Added Colors Customization ).
* Fixed : Notification Block Dismiss Issue.

= 1.2.1 =

* Fixed : Accordion Duplicate Bug.

= 1.2.0 =

* Added : Accordion / FAQ Block.
* Code Improvements.

= 1.1.0 =

* Added : Posts Grid Block.
* Code Improvements.

= 1.0.2 =

* Responsive Improvements.

= 1.0.1 =

* Added : Testimonial Block.
* Added : Profile Block.
* Code Improvements.
