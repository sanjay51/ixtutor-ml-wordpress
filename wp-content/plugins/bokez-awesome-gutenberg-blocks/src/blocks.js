/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

/** Blocks */
import './block/blockquote';
import './block/button';
import './block/cover';
import './block/divider';
import './block/notification';
import './block/separator-heading';
import './block/progress-bar';
import './block/share';

/** since 1.0.1 */
import './block/profile';
import './block/posts-grid';

// /** since 1.2.0 */
import './block/faq';

// /** since 1.3.0 */
import './block/testimonial';

// /** since 1.4.0 */
import './block/pricing-table';

// /** since 1.5.0 */
import './block/video-popup';

/** since 1.8.0 */
//import './block/row';
//import './block/column';
