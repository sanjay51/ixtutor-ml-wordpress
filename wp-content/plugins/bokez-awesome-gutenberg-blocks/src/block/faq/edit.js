import { bokez } from '../../global'
import { DeleteIcon } from '../../icons';

const { __ } = wp.i18n;
const { Component } = wp.element;

const {
    PanelColorSettings,
    InspectorControls, 
    AlignmentToolbar, 
    BlockControls,
    RichText,
} = wp.editor;

export class Edit extends Component{

    render(){
        
        const { 
            isSelected, 
            setAttributes, 
            attributes,
        } = this.props
    
        const { 
            items,
            alignment,
            headerTextColor,
            headerBgColor,
            bodyBgColor,
            bodyTextColor,
        } = attributes
    
        /**
         * Update Faq when changed
         * @param {number} index 
         * @param {string} key 
         * @param {string} newValue 
         */
        const faqsUpdate = ( index, key, newValue ) => {
    
            const items = attributes.items;
    
            return items.map( function( item, currIndex ) {
                if( index == currIndex ){
                    item[ key ] = newValue;
                }
    
                return item;
            });
    
        }
    
        const rows = items.map( ( item, index ) => {
    
            return (
    
                <div 
                    key = { 'bokez-item-' + index }
                    data-id = { index } 
                    className = { 'bokez-accordion-item' } 
                    onClick = { ( event ) => { 
                        jQuery('.bokez-accordion .bokez-accordion-item').removeClass('_active_bokez');
                        jQuery( event.currentTarget ).addClass('_active_bokez');
                    } } >
                    
                    <div className = { '_item_controls_bokez' }>
    
                        <span onClick = { ( event ) => {
                            return setAttributes({ items: items.filter(function( citem, cindex ){
                                    return cindex != event.currentTarget.getAttribute('data-index');
                                }) 
                            })
                        } } data-index = { index } ><DeleteIcon/></span>
                                        
                    </div>
    
                    <RichText      
                        style = {{ 'background-color' : headerBgColor, 'color' : headerTextColor }}
                        className = { '_item_header_bokez' }
                        formattingControls = {[]}
                        format = { 'string' }               
                        value = { item.header }
                        placeholder = { item.header }
                        tagName = { 'span' }
                        keepPlaceholderOnFocus = { true }
                        onChange = { ( newtext ) => {
                            return setAttributes({ items: faqsUpdate( index, 'header', newtext ) });
                        }}
                        onSplit = { () => null }
                    />
    
                    <RichText        
                        style = {{ 'background-color' : bodyBgColor, 'color' : bodyTextColor }}
                        className = { '_item_body_bokez' }
                        format = { 'string' }               
                        value = { item.body }
                        placeholder = { item.body }
                        tagName = { 'p' }
                        keepPlaceholderOnFocus = { true }
                        onChange = { ( newtext ) => {
                            return setAttributes({ items: faqsUpdate( index, 'body', newtext ) });
                        }}
                        onSplit = { () => null }
                    />
    
                </div>
    
            )
        });
    
        return [
    
            isSelected && (
                
                <BlockControls key = { 'controls' }>
                
                    <AlignmentToolbar
                        value={alignment}
                        onChange={ ( nextAlign ) => setAttributes( { alignment: nextAlign } ) }
                    />
                
                </BlockControls>
    
            ),
    
            isSelected && (
    
                <InspectorControls key = {'inspector'} > 
    
                    <hr/>
    
                    <PanelColorSettings 
                        title = { __( 'Header Background Color' ) } 
                        initialOpen = { false } 
                        colorValue = { headerBgColor } 
                        colorSettings={ [ {
                                value: headerBgColor,
                                colors: bokez.colors,
                                label: __( 'Header Background Color' ),
                                onChange: ( newColor ) => setAttributes( { headerBgColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Header Text Color' ) } 
                        initialOpen = { false } 
                        colorValue = { headerTextColor } 
                        colorSettings={ [ {
                                value: headerTextColor,
                                colors: bokez.colors,
                                label: __( 'Header Text Color' ),
                                onChange: ( newColor ) => setAttributes( { headerTextColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Body Background Color' ) } 
                        initialOpen = { false } 
                        colorValue = { bodyBgColor }
                        colorSettings={ [ {
                                value: bodyBgColor,
                                colors: bokez.colors,
                                label: __( 'Body Background Color' ),
                                onChange: ( newColor ) => setAttributes( { bodyBgColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Body Text Color' ) } 
                        initialOpen = { false } 
                        colorValue = { bodyTextColor }
                        colorSettings={ [ {
                            value: bodyTextColor,
                            colors: bokez.colors,
                            label: __( 'Body Text Color' ),
                            onChange: ( newColor ) => setAttributes( { bodyTextColor: newColor } ),
                    } ] } >
                    </PanelColorSettings>
    
                </InspectorControls>
    
            ),
    
            <div key = { 'accordion' } className = { 'bokez-accordion bokez-accordion-' + alignment } style = {{ 'text-align': alignment }} >
                { rows }
            </div>,
    
            isSelected && (
                <div className = { 'bokez-accordion-item-add-wrapper' }>
                    <button 
                        className = { 'button button-primary' } 
                        onClick = { ( event ) => {
                            return setAttributes({ items: items.concat([{ header: 'New Header', body: 'New Body' }]) });
                        } } 
                    >
                        { __('Add Item') }
                    </button>
                </div>
            )
    
        ];
    }

}
