//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

// Import JS
import { Edit } from './edit';
import { FaqIcon } from '../../icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

const {
    RichText,
} = wp.editor;

registerBlockType( 'bokez/faq', {

    title: __( 'FAQ / Accordion' ),
    
    description: __('Easily create an accordion for your post / page.'),
    
    keywords: [__('accordion'), __('list'), __('faq')],
    
    icon: FaqIcon,
    
    category: 'bokez',
    
    attributes: {

        items:{
            type: 'array',
            default: [
                {
                    'header' : 'Header 1',
                    'body' : 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.',
                },
                {
                    'header' : 'Header 2',
                    'body' : 'Body 2',
                },
                {
                    'header' : 'Header 3',
                    'body' : 'Body 3',
                }
            ]
        },
    
        alignment: {
            type: 'string',
            default: 'left'
        },
    
        headerTextColor: {
            type: 'string',
            default: ''
        },
    
        headerBgColor: {
            type: 'string',
            default: ''
        },
    
        bodyTextColor: {
            type: 'string',
            default: ''
        },
    
        bodyBgColor: {
            type: 'string',
            default: ''
        },
    
    },
    
    edit: Edit,
    
    save: ( props ) => {

        const { 
            items,
            alignment,
            headerBgColor,
            headerTextColor,
            bodyBgColor,
            bodyTextColor,
        } = props.attributes
        
        const rows = items.map( ( item, index ) => {
            return (
    
                <div 
                    data-id = { index } 
                    className = { 'bokez-accordion-item' } >
    
                    <RichText.Content 
                        style = {{ 'background-color' : headerBgColor, 'color' : headerTextColor }}
                        className = { '_item_header_bokez' }
                        value = { item.header }
                        tagName = { 'span' }
                    />
    
                    <RichText.Content
                        style = {{ 'background-color' : bodyBgColor, 'color' : bodyTextColor }}
                        className = { '_item_body_bokez' }
                        value = { item.body }
                        tagName = { 'p' }
                    />
    
                </div>
    
            )
        });
    
        return (
    
            <div key = { 'accordion' } className = { 'bokez-accordion bokez-accordion-' + alignment } style = {{ 'text-align': alignment }} >
                { rows }
            </div>
            
        );
        
    },

});
