/**
 * BLOCK: bokez-gutenberg
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';
import { bokez } from '../../global';
import { TestimonialIcon, DeleteIcon, UploadImageIcon, ArrowLeftIcon, ArrowRightIcon } from '../../icons';

const { Fragment, Component } = wp.element;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

const {
    InspectorControls, 
    AlignmentToolbar, 
    BlockControls,
    RichText,
    MediaUpload,
    InnerBlocks,    
} = wp.editor;

const { 
    PanelColor,	
    ColorPalette,
    Button,
    ToggleControl
} = wp.components;

const blockAttributes = {

    alignment: {
        type: 'string',
        default: 'center'
    },

};

export class BokezRowBlock extends Component{

	/**
	 * Constructor.
	 * Sets up state, and creates bindings for functions.
	 * @param object props - current component properties.
	 */
	constructor( props ){
		super(...arguments);
		this.props = props;

        let self = this;
    }

	/**
	 * When the component mounts it calls this function.
	 */
	componentDidMount() {}
    
    /**
     * When the component updates
     * @param {object} prevProps 
     * @param {object} prevState 
     */
    componentDidUpdate(prevProps, prevState) {}

    /**
     * Returns columns array.
     *
     * @param {number} columns Number of columns.
     *
     * @return {Object}
     */
    getColumnsTemplate( columns ){
        
        let arr = [];

        for ( let index = 1; index <= columns; index++) {
            arr.push( [ 'bokez/column', { id: index } ] );
        }
        
        return arr;
    }

    /**
	 * Renders the component.
	**/
    render(){        
        let self = this;

        const { 
            isSelected, 
            setAttributes,
        } = self.props
    
        return [
            <div 
                className= { "bokez-row" }
                data-columns = { 2 }
            >
                <InnerBlocks
                    template={ self.getColumnsTemplate( 2 ) }
                    templateLock="all"
                    allowedBlocks={ [ 'bokez/column' ] } />
            </div>
        ];
    };
}

registerBlockType( 'bokez/row', {
    
    title: __( 'Row Layout' ),
        
    keywords: [ __('row'), __('section'), __('layout') ],
    
    icon: TestimonialIcon,
    
    category: 'bokez',
    
    attributes: blockAttributes,
    
    edit: BokezRowBlock,

    save: ( props ) => {

        return (
    
            <div 
                className= { "bokez-row" }
            >
                <InnerBlocks.Content />
            </div>

        );
    },

})

