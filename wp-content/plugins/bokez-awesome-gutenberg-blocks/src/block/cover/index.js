//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

// Import JS
import { Edit } from './edit';
import { CoverIcon } from '../../icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;

registerBlockType( 'bokez/cover-1', {
    
    title: __( 'Cover One' ),
    
    description: __('Call to action or Cover ? We got you covered...'),
    
    keywords: [__('call to action'), __('cover'), __('header')],
    
    icon: CoverIcon,
    
    category: 'bokez',
    
    attributes: {
        uid: {
            type: 'string',
            default: ''
        },
        alignment: {
            type: 'string',
            default: 'center'
        },
        imageUrl: {
            type: 'string',
        },
        imageID: {
            type: 'number',
        },
        title: {
            type: 'string',
            default: 'John & Dina'
        },
        subtitle: {
            type: 'string',
            default: 'We have combined many pre-designed blocks which you will need when building your pages.'
        },
        buttonText:{
            type: 'string',
            default: 'Buy Now'
        },
        buttonUrl:{
            type: 'string',
            default: 'http://delabon.com'
        },
        backgroundColor:{
            type: 'string',
        },
        textColor:{
            type: 'string',
            default: '#010101'
        },
        buttonColor:{
            type: 'string',
            default: '#ffffff'
        },
        buttonBGColor:{
            type: 'string',
        },
        showHero: {
            type: 'boolean',
            default: true
        },
        showButton: {
            type: 'boolean',
            default: true
        }
    },
    
    edit: Edit,
    
    save: ( props ) => {
    
        const {
            alignment,
            imageUrl,
            imageID,
            title,
            subtitle,
            buttonText,
            buttonUrl,
            backgroundColor,
            textColor,
            buttonColor,
            buttonBGColor,
            showButton,
            showHero,
            uid
        } = props.attributes;
    
        const style = { 
            'text-align' : alignment, 
            'background-color' : backgroundColor,
        };
    
        if( imageUrl ){
            style['background-image'] = 'url('+ imageUrl +')' ;
        } 
    
        return (
            <div id = { uid } key={ 'cover-one' } className={ 'bokez-cover bokez-cover-one' + ( imageID ? ' bokez-cover-has-image' : ''  ) } style = { style } >            
                
                <div className = 'bokez-cover-content' >
                
                    <RichText.Content
                        style = {{
                            'color' : textColor,
                        }}
                        tagName = "h2"
                        className = { 'bokez-cover-title' }
                        value = { title }
                    />
                    
                    { showHero && (
                        <RichText.Content
                            style = {{
                                'color' : textColor,
                            }}
                            tagName = "p"
                            className = { 'bokez-cover-subtitle' }
                            value = { subtitle }
                        />
                    )}
                    
                    { showButton && (
                        <RichText.Content
                            style = {{
                                'color' : buttonColor,
                                'background-color' : buttonBGColor,
                            }}
                            tagName = "a"
                            className = { 'bokez-cover-button' }
                            href = { buttonUrl }
                            value = { buttonText }
                        />
                    )}
    
                </div>
                
            </div>
    
        );
        
    },
});
