//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

// Import JS
import { Edit } from './edit';
import { DividerIcon } from '../../icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'bokez/divider', {
    
    title: __( 'Divider' ),
    
    description: __('Insert a horizontal line where you want to create a break between ideas.'),
    
    keywords: [__('divider'), __('separator')],
    
    icon: DividerIcon,
    
    category: 'bokez',
    
    attributes: {
        alignment: {
            type: 'string',
        },
        borderColor: {
            type: 'string',
            default: '#e1e1e1',
        },
        borderSize: {
            type: 'number',
            default: 1,
        },
        borderType: {
            type: 'string',
            default: 'solid',
        },
        width: {
            type: 'number',
            default: 50,
        },
    },
    
    edit: Edit,
    
    save: ( props ) => {

        const { 
            alignment, 
            borderSize, 
            borderColor,
            borderType, 
            width       
        } = props.attributes
    
        return (
            <div key={ 'divider' } className={ 'bokez-divider' } style = { { 'text-align': alignment } } >
                <span style={ { 
                    'width' : width + '%', 
                    'border-width' : borderSize + 'px',
                    'border-color' : borderColor,
                    'border-style' : borderType,
                    } }
                ></span>
            </div>
        );
        
    },
})
