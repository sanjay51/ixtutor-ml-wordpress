//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

//  Import JS.
import { Edit } from './edit';
import { TestimonialIcon, ArrowLeftIcon, ArrowRightIcon } from '../../icons';

const { __ } = wp.i18n;
const { RichText } = wp.editor;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;

registerBlockType( 'bokez/testimonial', {
    
    title: __( 'Testimonial' ),
    
    description: __('Add a multiple testimonials with a name, title and text.'),
    
    keywords: [__('testimonial'), __('user')],
    
    icon: TestimonialIcon,
    
    category: 'bokez',
    
    attributes: {

        items: {
            type: 'array',
            default: [
                {
                    message: 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.',
                    name: 'John Doe',
                    job: 'Designer',
                    imageUrl: '',
                    imageID: 0,
                }
            ]
        },
    
        showPoints: {
            type: 'boolean',
            default: true
        },
    
        showArrows: {
            type: 'boolean',
            default: true
        },
    
        alignment: {
            type: 'string',
            default: 'center'
        },
    
        backgroundColor: {
            type: 'string',
            default: 'makeMeIgnored()',
        },
    
        textColor: {
            type: 'string',
            default: 'makeMeIgnored()',
        },
    
        arrowsColor: {
            type: 'string',
            default: '',
        },
    
        pointsColor: {
            type: 'string',
            default: '',
        },
    },
    
    edit: Edit,

    save: ( props ) => {
    
        const { 
            backgroundColor,
            textColor,
            alignment,
            items,
            arrowsColor,
            pointsColor,
            showPoints,
            showArrows,
        } = props.attributes
    
        /**
         * Create items ui
         */
        const rows = items.map( ( item, index ) => {
    
            return (
    
                <div 
                    key = { 'bokez-item-' + index }
                    data-id = { index } 
                    className = { '_item_bokez' + ( index === 0 ? ' _item_current_bokez' : '' ) } 
                >
    
                    { item.imageUrl && (
                        <div className = { '_item_avatar_wrapper_bokez' } >
    
                            <div className = { '_item_avatar_bokez' } >
                                <img src = { item.imageUrl } />
                            </div>
    
                        </div>
                    )}
                    
                    <RichText.Content      
                        className = { '_item_name_bokez' }
                        format = { 'string' }  
                        value = { item.name }
                        tagName = { 'h3' }
                    />
    
    
                    <RichText.Content        
                        className = { '_item_job_bokez' }
                        format = { 'string' }               
                        value = { item.job }
                        tagName = { 'span' }
                    />
    
                    <RichText.Content        
                        className = { '_item_message_bokez' }
                        format = { 'string' }               
                        value = { item.message }
                        tagName = { 'p' }
                    />
    
                </div>
    
            )
        });
    
        return (
    
            <div 
                key={ 'testimonials' } 
                className={ 'bokez-testimonials' } 
                style = {{ 
                    'text-align' : alignment,
                    'background-color' : backgroundColor,
                    'color' : textColor,
                }} 
            >
    
                { rows }
        
                { showPoints && items.length > 1 && (

                    <div className = {'_points_bokez'} >
                    
                        { items.map( ( item, index ) => {

                            return (
                                <span 
                                    style = {{ 'background-color' : pointsColor }}
                                    data-index = { index }
                                    className = { index === 0 ? '_current_point_bokez' : '' }
                                ></span>
                            );
        
                        }) }

                    </div>

                )}                
                
                { showArrows && items.length > 1 && (
                    <Fragment>
                        <div className = { '_arrow_left_bokez' } ><ArrowLeftIcon fill = { arrowsColor } /></div>
                        <div className = { '_arrow_right_bokez' } ><ArrowRightIcon fill = { arrowsColor } /></div>
                    </Fragment>
                ) }
    
            </div>
            
        );
    },

})

