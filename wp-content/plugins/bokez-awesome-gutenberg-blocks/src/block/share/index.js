//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

//  Import JS.
import { Edit, justifyContent } from './edit'
import { ShareIcon, FacebookIcon, TwitterIcon, GooglePlusIcon, RedditIcon, LinkedinIcon, EmailIcon } from '../../icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'bokez/share', {
    
    title: __( 'Share' ),
    
    description: __('Make your visitors share your content.'),
    
    keywords: [__('share'), __('facebook'), __('twitter')],
    
    icon: ShareIcon,
    
    category: 'bokez',
    
    attributes: {

        showFb: {
            type: 'boolean',
            default: true,
        },

        showTwitter:{
            type: 'boolean',
            default: true,
        },

        showGoogle:{
            type: 'boolean',
            default: true,
        },

        showReddit:{
            type: 'boolean',
            default: true,
        },

        showLinkedin:{
            type: 'boolean',
            default: true,
        },

        showEmail:{
            type: 'boolean',
            default: true,
        },

        alignment: {
            type: 'string',
            default: 'center'
        },

        bgColor: {
            type: 'string',
            default: ''
        },

        iconColor: {
            type: 'string',
            default: ''
        }
    },
    
    edit: Edit,
    
    save: ( props ) => {

        const { 
            showFb,
            showGoogle,
            showLinkedin,
            showReddit,
            showTwitter,
            showEmail,
            alignment,
            bgColor,
            iconColor,
        } = props.attributes
    
        return (
            <div key = { 'button' } className = { 'bokez-share' } style = { { 'justify-content': justifyContent( alignment ) } } >
                
                { showFb && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-facebook' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Facebook') }
                        </span>
    
                        <FacebookIcon fill = { iconColor } />
                    </a>
                )}
    
                { showTwitter && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-twitter' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Twitter') }
                        </span>
    
                        <TwitterIcon fill = { iconColor } />
                    </a>
                )}
                
                { showGoogle && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-google' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Google+') }
                        </span>
    
                        <GooglePlusIcon fill = { iconColor } />
                    </a>
                )}
    
                { showReddit && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-reddit' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Reddit') }
                        </span>
    
                        <RedditIcon fill = { iconColor } />
                    </a>
                )}
    
                { showLinkedin && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-linkedin' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Linkedin') }
                        </span>
    
                        <LinkedinIcon fill = { iconColor } />
                    </a>
                )}
    
                { showEmail && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-email' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Email') }
                        </span>
    
                        <EmailIcon fill = { iconColor } />
                    </a>
                )}
    
            </div>
        );
        
    },
});
