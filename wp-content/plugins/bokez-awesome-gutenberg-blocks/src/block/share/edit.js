import { bokez } from '../../global'
import { ShareIcon, FacebookIcon, TwitterIcon, GooglePlusIcon, RedditIcon, LinkedinIcon, EmailIcon } from '../../icons';

const { __ } = wp.i18n;
const { Component } = wp.element;

const {
    PanelColorSettings,	
    InspectorControls, 
    AlignmentToolbar, 
    BlockControls,
} = wp.editor;

const { 
    ToggleControl,
} = wp.components;

export const justifyContent = ( alignment ) => {

    if( alignment === 'left' ){
        return 'flex-start';
    }
    else if( alignment === 'right' ){
        return 'flex-end';
    }
    
    return 'center';
}

export class Edit extends Component{

    render(){

        const { 
            isSelected, 
            setAttributes, 
            attributes
        } = this.props
    
        const { 
            showFb,
            showGoogle,
            showLinkedin,
            showReddit,
            showTwitter,
            showEmail,
            alignment,
            bgColor,
            iconColor,
        } = attributes
    
        return [
    
            <div key = { 'button' } className = { 'bokez-share' } style = { { 'justify-content': justifyContent( alignment ) } } >
                
                { showFb && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        onClick = { (event) => { event.preventDefault(); event.stopPropagation(); } } 
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-facebook' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Facebook') }
                        </span>
    
                        <FacebookIcon fill = { iconColor } />
                    </a>
                )}
    
                { showTwitter && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        onClick = { (event) => { event.preventDefault(); event.stopPropagation(); } } 
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-twitter' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Twitter') }
                        </span>
    
                        <TwitterIcon fill = { iconColor } />
                    </a>
                )}
    
                { showGoogle && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        onClick = { (event) => { event.preventDefault(); event.stopPropagation(); } } 
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-google' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Google+') }
                        </span>
    
                        <GooglePlusIcon fill = { iconColor } />
                    </a>
                )}
    
                { showReddit && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        onClick = { (event) => { event.preventDefault(); event.stopPropagation(); } } 
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-reddit' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Reddit') }
                        </span>
    
                        <RedditIcon fill = { iconColor } />
                    </a>
                )}
    
                { showLinkedin && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        onClick = { (event) => { event.preventDefault(); event.stopPropagation(); } } 
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-linkedin' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Linkedin') }
                        </span>
    
                        <LinkedinIcon fill = { iconColor } />
                    </a>
                )}
    
                { showEmail && ( 
                    <a 
                        style = {{ 'background-color' : bgColor }}
                        onClick = { (event) => { event.preventDefault(); event.stopPropagation(); } } 
                        href = 'javascript:void(0)'
                        className = { 'bokez-share-email' }                                 
                    >
                        <span className = 'bokez-share-tooltip' >
                            { __('Email') }
                        </span>
    
                        <EmailIcon fill = { iconColor } />
                    </a>
                )}
    
            </div>,
            
            isSelected && (
    
                <BlockControls key = { 'controls' }>
                    <AlignmentToolbar
                        value={alignment}
                        onChange={ ( nextAlign ) => setAttributes( { alignment: nextAlign } ) }
                    />
                </BlockControls>
    
            ),
    
            isSelected && (
    
                <InspectorControls key = {'inspector'} > 
    
                    <hr/>
    
                    <ToggleControl
                        label = { __( 'Facebook' ) }
                        checked = { showFb }
                        onChange = { ( value ) => setAttributes( { showFb: value } ) }
                    />
    
                    <ToggleControl
                        label = { __( 'Twitter' ) }
                        checked = { showTwitter }
                        onChange = { ( value ) => setAttributes( { showTwitter: value } ) }
                    />
    
                    <ToggleControl
                        label = { __( 'Google+' ) }
                        checked = { showGoogle }
                        onChange = { ( value ) => setAttributes( { showGoogle: value } ) }
                    />
    
                    <ToggleControl
                        label = { __( 'Reddit' ) }
                        checked = { showReddit }
                        onChange = { ( value ) => setAttributes( { showReddit: value } ) }
                    />
    
                    <ToggleControl
                        label = { __( 'Linkedin' ) }
                        checked = { showLinkedin }
                        onChange = { ( value ) => setAttributes( { showLinkedin: value } ) }
                    />
    
                    <ToggleControl
                        label = { __( 'Email' ) }
                        checked = { showEmail }
                        onChange = { ( value ) => setAttributes( { showEmail: value } ) }
                    />
    
                    <PanelColorSettings 
                        title = { __( 'Background Color' ) } 
                        initialOpen = { false } 
                        colorValue = { bgColor } 
                        colorSettings={ [ {
                            value: bgColor,
                            colors: bokez.colors,
                            label: __( 'Background Color' ),
                            onChange: ( newColor ) => setAttributes( { bgColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Icon Color' ) } 
                        initialOpen = { false } 
                        colorValue = { iconColor } 
                        colorSettings={ [ {
                            value: iconColor,
                            colors: bokez.colors,
                            label: __( 'Icon Color' ),
                            onChange: ( newColor ) => setAttributes( { iconColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                </InspectorControls>
    
            )
            
        ];
    }

}