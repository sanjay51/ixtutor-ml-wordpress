//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

// Import JS
import { ButtonIcon } from '../../icons';
import { Edit } from './edit';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

const {
    RichText,
} = wp.editor;

registerBlockType( 'bokez/button', {

    title: __( 'Button' ),
    
    description: __('Subscribe, buy, or read more? Add one now.'),
    
    keywords: [__('button'), __('link')],
    
    icon: ButtonIcon,
    
    category: 'bokez',

    attributes: {
        text:{
            type: 'string',
            default: 'Buy Now',
        },
        url:{
            type: 'url',
            default: 'http://delabon.com',
        },
        alignment: {
            type: 'string',
        },
        color: {
            type: 'string',
            default: '#e1e1e1',
        },
        backgroundColor: {
            type: 'string',
            default: 'black',
        },
        size: {
            type: 'string',
            default: 'default',            
        },
        type: {
            type: 'string',
            default: 'calltoaction',            
        },
    
    },

    edit: Edit,

    save: ( props ) => {

        const { 
            text,
            url,
            alignment,
            color,
            backgroundColor,
            type,
            size
        } = props.attributes
    
        return (
            <div key = { 'button' } className = { 'bokez-button' } style = { { 'text-align': alignment } } >
                <a 
                    href = { url } 
                    style = {{ 
                        'background-color': backgroundColor, 
                        'color' : color,
                    }} 
                    className = { 'bokez-button-type-' + type + ' bokez-button-size-' + size }                                 
                >
                    
                    <RichText.Content    
                        value = { text }
                        tagName = { 'span' }
                    />
                    
                </a>
            </div>
        );
        
    },
})
