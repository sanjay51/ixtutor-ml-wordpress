//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

// Import JS
import { Edit } from './edit'
import { ProgressBarIcon } from '../../icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'bokez/progress-bar', {

    title: __( 'Progress Bar' ),
    
    description: __('Add beautiful customized progress bar.'),
    
    keywords: [__('progress'), __('bar'), __('skill')],
    
    icon: ProgressBarIcon,
    
    category: 'bokez',
    
    attributes: {
        progress: {
            type: 'string',
            default: '50%'
        },
        backgroundColor: {
            type: 'string',
            default: '#f5f5f5'
        },
        progressColor: {
            type: 'string',
            default: 'rgb(23,44,60)'
        },
        tooltipColor: {
            type: 'string',
            default: 'rgb(23,44,66)'
        },
        tooltipBackgroundColor: {
            type: 'string',
            default: '#eebf3f'
        },
    },
    
    edit: Edit,
    
    save: ( props ) => {
    
        const {
            progress,
            progressColor,
            backgroundColor,
            tooltipBackgroundColor,
            tooltipColor
        } = props.attributes;
    
        return (
    
            <div key={ 'progress-bar' } className={ 'bokez-progress-bar-wrapper' } >
                
                <div className = { 'bokez-progress-bar' } style = { { 'background-color': backgroundColor } } >
                
                    <div className = { 'bokez-progress-bar-progress' } style = { { 'background-color': progressColor , 'width' : progress } } >
                        
                        <div className = { 'bokez-progress-bar-tooltip' } style = { { 'background-color': tooltipBackgroundColor , 'color' : tooltipColor } } >{ progress }
                            
                            <span style = { { 'border-top-color': tooltipBackgroundColor } } ></span>
                        
                        </div>
                    
                    </div>  
                
                </div>
    
            </div>
        
        );
        
    },
})

