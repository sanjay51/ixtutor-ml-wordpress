/**
 * BLOCK: bokez-gutenberg
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

import { bokez } from '../../global';
import { TestimonialIcon, DeleteIcon, UploadImageIcon, ArrowLeftIcon, ArrowRightIcon } from '../../icons';

const { Fragment, Component } = wp.element;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

const {
    InspectorControls, 
    AlignmentToolbar, 
    BlockControls,
    RichText,
    MediaUpload,
    InnerBlocks,    
} = wp.editor;

const { 
    PanelColor,	
    ColorPalette,
    Button,
    ToggleControl
} = wp.components;

const blockAttributes = {

    id: {
        type: 'number',
        default: 1
    },

    alignment: {
        type: 'string',
        default: 'center'
    },

};

export class BokezColumnBlock extends Component{

	/**
	 * Constructor.
	 * Sets up state, and creates bindings for functions.
	 * @param object props - current component properties.
	 */
	constructor( props ){
		super(...arguments);
		this.props = props;

        let self = this;
    }

	/**
	 * When the component mounts it calls this function.
	 */
	componentDidMount() {}
    
    /**
     * When the component updates
     * @param {object} prevProps 
     * @param {object} prevState 
     */
    componentDidUpdate(prevProps, prevState) {}

    /**
	 * Renders the component.
	**/
    render(){        
        let self = this;

        const { 
            isSelected, 
            setAttributes, 
        } = self.props
    
        const { 
            id,
            alignment,
        } = self.props.attributes
    
        return [
            <div 
                className={ `bokez-column bokez-column-${id}` } 
            >
                <div 
                    className="bokez-column-inner" 
                >
					<InnerBlocks templateLock={ false } />
				</div>
			</div>
        ];
    };
}

registerBlockType( 'bokez/column', {
    
    title: __( 'Column' ),
        
    keywords: [ __('column'), __('section'), __('layout') ],
    
    icon: TestimonialIcon,
    
    category: 'bokez',
    
    attributes: blockAttributes,
    
    edit: BokezColumnBlock,

    save: ( props ) => {
    
        const { 
            id,
        } = props.attributes
    
        return (
    
            <div 
                className={ `bokez-column bokez-column-${id}` } 
            >
                <div 
                    className="bokez-column-inner" 
                >
					<InnerBlocks.Content />
				</div>
			</div>

        );
    },

})

