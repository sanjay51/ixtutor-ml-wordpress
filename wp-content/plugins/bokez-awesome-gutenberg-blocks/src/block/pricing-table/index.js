//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

//  Import JS.
import { Edit, styleGen } from './edit';
import { PricingTableIcon } from '../../icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;

registerBlockType( 'bokez/pricing-table', {
    
    title: __( 'Pricing Table' ),
    
    description: __('Add Pricing Boxes.'),
    
    keywords: [ __('price'), __('table'), __('tag') ],
    
    icon: PricingTableIcon,
    
    category: 'bokez',
    
    attributes: {
    
        ID: {
            type: 'string',
            default: '',
        },
    
        items: {
            type: 'array',
            default: [
                {
                    imageUrl: '',
                    imageID: 0,
                    header: 'SMALL TEAM',
                    features:`
                            <li>Custom Domains</li> 
                            <li>5 Users</li>
                            <li>10 Projects</li>
                    `,
                    price: '$49',
                    buttonName: 'Free Trial',
                    buttonUrl: 'http://delabon.com/',
                }
            ]
        },
    
        backgroundColor: {
            type: 'string',
            default: '',
        },
    
        titleColor: {
            type: 'string',
            default: '',
        },
    
        featuresColor: {
            type: 'string',
            default: '',
        },
        
        priceColor: {
            type: 'string',
            default: '',
        },
        
        buttonColor: {
            type: 'string',
            default: '',
        },
    
        buttonBGColor: {
            type: 'string',
            default: '',
        },
    
        buttonColorHover: {
            type: 'string',
            default: '',
        },
    },
    
    edit: Edit,
    
    save: ( props ) => {

        const { 
            className,
            attributes,
        } = props;
    
        const { 
            ID,
            items,
        } = attributes;
    
        /**
         * Create items ui
         */
        const rows = items.map( ( item, index ) => {
    
            return (
    
                <div 
                    key = { 'bokez-item-' + index }
                    data-id = { index } 
                    className = { '_item_bokez' } 
                >
                    
                    { item.imageUrl && (
                        <div className = { '_item_avatar_wrapper_bokez' } >
    
                            <div className = { '_item_avatar_bokez' } >
                                <img src = { item.imageUrl } />
                            </div>
    
                        </div>
                    )}
    
                    <RichText.Content      
                        className = { '_item_header_bokez' }
                        format = { 'string' }  
                        value = { item.header }
                        tagName = { 'h3' }
                    />
    
                    <RichText.Content        
                        className = { '_item_features_bokez' }
                        format = { 'string' }               
                        value = { item.features }
                        tagName = { 'ul' }
                        multiline = { 'li' }
                    />
    
                    <RichText.Content
                        className = { '_item_price_bokez' }
                        format = { 'string' }               
                        value = { item.price }
                        tagName = { 'span' }
                    />
    
                    <RichText.Content 
                        className = { '_item_button_bokez' }
                        format = { 'string' }               
                        value = { item.buttonName }
                        tagName = { 'a' }
                        href = { item.buttonUrl }
                    />
    
                </div>
    
            )
        });
    
        return (
    
            <div>{ styleGen( attributes ) }<div 
                    key = { 'pricing-table' } 
                    id = { ID }
                    className = { 'bokez-pricing-table _' + items.length + '_columns_bokez' } 
                >
    
                    { rows }
    
                </div>
                
            </div>
    
            
        );
    },
})
