import { bokez } from '../../global';
import { DeleteIcon, UploadImageIcon } from '../../icons';

const { __ } = wp.i18n;
const { Component } = wp.element;

const {
    PanelColorSettings,
    InspectorControls, 
    RichText,
    MediaUpload,
    URLInput,   
} = wp.editor;

const { 
    Button,
	Dashicon,    
} = wp.components;

/**
 * HTML Style
 * @param {Object} attributes 
 */
export const styleGen = ( attributes ) => {
    
    const { 
        ID,
        backgroundColor,
        titleColor,    
        featuresColor,
        priceColor,
        buttonColor,
        buttonBGColor,
        buttonColorHover
    } = attributes;

    return (
        
        <style>{`
            #${ID} ._item_bokez {
                background-color: ${ backgroundColor ? backgroundColor : '' };
            }

            #${ID} ._item_header_bokez {
                color: ${ titleColor ? titleColor : '' };
            }

            #${ID} ._item_features_bokez {
                color: ${ featuresColor ? featuresColor : '' };
            }

            #${ID} ._item_features_bokez li {
                border-color: ${ featuresColor ? featuresColor : '' };
            }

            #${ID} ._item_price_bokez {
                color: ${ priceColor ? priceColor : '' };
            }

            #${ID} ._item_button_bokez {
                color: ${ buttonColor ? buttonColor : '' };
                border-color: ${ buttonColor ? buttonColor : '' };
            }

            #${ID} ._item_button_bokez:hover {
                background-color: ${ buttonBGColor ? buttonBGColor : '' };
                border-color: ${ buttonBGColor ? buttonBGColor : '' };
                color: ${ buttonColorHover ? buttonColorHover : '' };
            }
        `}</style>

    );
}


export class Edit extends Component{

    render(){
    
        const { 
            isSelected, 
            setAttributes, 
            attributes,
        } = this.props;
    
        const { 
            ID,
            items,
            backgroundColor,
            titleColor,    
            featuresColor,
            priceColor,
            buttonColor,
            buttonBGColor,
            buttonColorHover,
        } = attributes;
    
        if( ID === '' ){
            setAttributes({ ID : bokez.uniqueID() });
        }
    
        /**
         * Update Faq when changed
         * @param {number} index 
         * @param {string} key 
         * @param {string} newValue 
         */
        const faqsUpdate = ( index, key, newValue ) => {
    
            const items = attributes.items;
    
            return items.map( function( item, currIndex ) {
                if( index == currIndex ){
                    item[ key ] = newValue;
                }
    
                return item;
            });
    
        }
    
        /**
         * Create items ui
         */
        const rows = items.map( ( item, index ) => {
    
            return (
    
                <div 
                    key = { 'bokez-item-' + index }
                    data-id = { index } 
                    className = { '_item_bokez' } 
                >
                    
                    <div className = { '_item_controls_bokez' }>
    
                        <span onClick = { ( event ) => {
                            return setAttributes({ items: items.filter(function( citem, cindex ){
                                    return cindex != event.currentTarget.getAttribute('data-index');
                                }) 
                            })
                        } } data-index = { index } ><DeleteIcon/></span>
                                        
                    </div>
    
                    <MediaUpload
                        className = {'bokez-cover-upload'}
                        onSelect = { ( media ) => {
                            
                            let newItems = faqsUpdate( index, 'imageUrl', media.url );
                            newItems = faqsUpdate( index, 'imageID', media.id );
    
                            return setAttributes({ items: newItems });
                        }}
                        type = { 'image' }
                        value = { item.imageID }
                        render = { function( obj ) {
                            return (
    
                                ! item.imageUrl ? (
                                    <Button
                                        className = { item.imageID ? '' : 'button-bokez-image-add button button-large' }
                                        onClick = { obj.open } 
                                    >
                                        <UploadImageIcon/>
                                    </Button>
                                ) : (
            
                                    <div className = { '_item_avatar_wrapper_bokez' } >
    
                                        <div className = { '_item_avatar_bokez' } >
                                            <img src = { item.imageUrl } onClick = { obj.open }  />
                                        </div>
                                    </div>
    
                                )
    
                            )
                        } }
                    />
                    
                    <RichText      
                        className = { '_item_header_bokez' }
                        formattingControls = {[]}
                        format = { 'string' }  
                        value = { item.header }
                        placeholder = { item.header }
                        tagName = { 'h3' }
                        keepPlaceholderOnFocus = { true }
                        onChange = { ( newtext ) => {
                            return setAttributes({ items: faqsUpdate( index, 'header', newtext ) });
                        }}
                        onSplit = { () => null }
                    />
    
                    <RichText        
                        className = { '_item_features_bokez' }
                        format = { 'string' }               
                        value = { item.features }
                        placeholder = { item.features }
                        tagName = { 'ul' }
                        multiline = { 'li' }
                        keepPlaceholderOnFocus = { true }
                        onChange = { ( newtext ) => {
                            return setAttributes({ items: faqsUpdate( index, 'features', newtext ) });
                        }}
                    />
    
                    <RichText        
                        className = { '_item_price_bokez' }
                        formattingControls = {[]}
                        format = { 'string' }               
                        value = { item.price }
                        placeholder = { item.price }
                        tagName = { 'span' }
                        keepPlaceholderOnFocus = { true }
                        onChange = { ( newtext ) => {
                            return setAttributes({ items: faqsUpdate( index, 'price', newtext ) });
                        }}
                        onSplit = { () => null }
                    />
    
                    <RichText        
                        className = { '_item_button_bokez' }
                        formattingControls = {[]}
                        format = { 'string' }               
                        value = { item.buttonName }
                        placeholder = { item.buttonName }
                        tagName = { 'a' }
                        keepPlaceholderOnFocus = { true }
                        onChange = { ( newtext ) => {
                            return setAttributes({ items: faqsUpdate( index, 'buttonName', newtext ) });
                        }}
                        onSplit = { () => null }
                        href = { item.buttonUrl }
                        onClick = { ( event ) => event.preventDefault() }
                    />
                    
                    <form
                        key={ 'form-link' }
                        onSubmit={ ( event ) => event.preventDefault() }
                        className={ 'bokez-block-form blocks-button__inline-link' }
                    >
                        <Dashicon icon={ 'admin-links' } />
                        <URLInput
                            value={ item.buttonUrl }
                            onChange={ ( newtext ) => {
                                return setAttributes({ items: faqsUpdate( index, 'buttonUrl', newtext ) });
                            }}
                        />
                    </form>
    
                </div>
    
            )
        });
    
        return [
    
            isSelected && (
    
                <InspectorControls key = {'inspector'} > 
    
                    <hr/>
                
                    <PanelColorSettings 
                        title = { __( 'Background Color' ) } 
                        initialOpen = { false } 
                        colorValue = { backgroundColor } 
                        colorSettings={ [ {
                                value: backgroundColor,
                                colors: bokez.colors,
                                label: __( 'Background Color' ),
                                onChange: ( newColor ) => setAttributes( { backgroundColor: newColor } ),
                        } ] } >
    
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Title Color' ) } 
                        initialOpen = { false } 
                        colorValue = { titleColor } 
                        colorSettings={ [ {
                                value: titleColor,
                                colors: bokez.colors,
                                label: __( 'Title Color' ),
                                onChange: ( newColor ) => setAttributes( { titleColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Features Color' ) } 
                        initialOpen = { false } 
                        colorValue = { featuresColor }
                        colorSettings={ [ {
                                value: featuresColor,
                                colors: bokez.colors,
                                label: __( 'Features Color' ),
                                onChange: ( newColor ) => setAttributes( { featuresColor: newColor } ),
                        } ] } >
    
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Price Color' ) } 
                        initialOpen = { false } 
                        colorValue = { priceColor }
                        colorSettings={ [ {
                                value: priceColor,
                                colors: bokez.colors,
                                label: __( 'Price Color' ),
                                onChange: ( newColor ) => setAttributes( { priceColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Button Color' ) } 
                        initialOpen = { false } 
                        colorValue = { buttonColor } 
                        colorSettings={ [ {
                                value: buttonColor,
                                colors: bokez.colors,
                                label: __( 'Button Color' ),
                                onChange: ( newColor ) => setAttributes( { buttonColor: newColor } ),
                        } ] } >
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Button Background Color ( Hover )' ) } 
                        initialOpen = { false } 
                        colorValue = { buttonBGColor }
                        colorSettings={ [ {
                                value: buttonBGColor,
                                colors: bokez.colors,
                                label: __( 'Button Background Color ( Hover )' ),
                                onChange: ( newColor ) => setAttributes( { buttonBGColor: newColor } ),
                        } ] } >
    
                    </PanelColorSettings>
    
                    <PanelColorSettings 
                        title = { __( 'Button Color ( Hover )' ) } 
                        initialOpen = { false } 
                        colorValue = { buttonColorHover } 
                        colorSettings={ [ {
                                value: buttonColorHover,
                                colors: bokez.colors,
                                label: __( 'Button Color ( Hover )' ),
                                onChange: ( newColor ) => setAttributes( { buttonColorHover: newColor } ),
                        } ] } >
    
                    </PanelColorSettings>
    
                </InspectorControls>
    
            ),
    
            <div>{ styleGen( attributes ) }<div 
                    key = { 'pricing-table' } 
                    id = { ID }
                    className = { 'bokez-pricing-table _' + items.length + '_columns_bokez' } 
                >
    
                    { rows }
    
                </div>
                
            </div>,
    
    
            isSelected && (
                <div className = { 'bokez-add-item-wrapper' }>
                    <button 
                        className = { 'button button-primary' } 
                        onClick = { ( event ) => {
    
                            if( items.length == 4 ) return;
    
                            return setAttributes({ items: items.concat([{
                                imageUrl: '',
                                imageID: 0,
                                header: 'ENTERPRISE',
                                features:`
                                        <li>Custom Domains</li> 
                                        <li>5 Users</li>
                                        <li>10 Projects</li>
                                `,
                                price: '$199',
                                buttonName: 'Free Trial',
                                buttonUrl: 'http://delabon.com/',
                            }]) });
                        } } 
                    >
                        { __('Add Item') }
                    </button>
                </div>
            )
            
        ];
    }

}

