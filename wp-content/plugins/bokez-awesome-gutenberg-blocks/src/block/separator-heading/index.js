//  Import CSS.
import './style/style.scss';
import './style/editor.scss';

//  Import JS.
import { Edit } from './edit'
import { SeparatorHeadingIcon } from '../../icons';

const { __ } = wp.i18n;
const { RichText } = wp.editor;
const { registerBlockType } = wp.blocks;

registerBlockType( 'bokez/separator-heading', {
    
    title: __( 'Separator With Text' ),
    
    description: __('Insert a horizontal line with text in the center.'),
    
    keywords: [__('divider'), __('separator')],
    
    icon: SeparatorHeadingIcon,
    
    category: 'bokez',
    
    attributes: {
        text: {
            type: 'string',
            default: 'Heading Goes Here',
            selector: 'h3',
        },
        textColor: {
            type: 'string',
            default: '#2d2d2d',            
        },
        borderColor: {
            type: 'string',
            default: '#e1e1e1',
        },
        borderSize: {
            type: 'number',
            default: 1,
        },
        borderType: {
            type: 'string',
            default: 'solid',
        },
        width: {
            type: 'number',
            default: 50,
        }
    },
    
    edit: Edit,
    
    save: ( props ) => {

        const { 
            text,
            textColor,
            borderSize,
            borderType,
            borderColor,
            width
        } = props.attributes
    
        return (
            <div className={ 'bokez-separator-heading-wrapper' } >
                
                <div className = { 'bokez-separator-heading' } style = { { width : width + '%' } } > 
    
                    <span 
                        className = { 'bokez-separator-heading-border' } 
                        style={ { 'border-bottom' : borderSize + 'px ' + borderType + ' ' + borderColor,	 } }
                    ></span>
    
                    <RichText.Content
                        tagName = { 'h3' }
                        value = { text }
                        style = {{ 'color' : textColor }}
                    />
                
                    <span 
                        className = { 'bokez-separator-heading-border' } 
                        style={ { 'border-bottom' : borderSize + 'px ' + borderType + ' ' + borderColor,	 } }
                    ></span>
    
                </div>
    
            </div>
        );
        
    },
})
