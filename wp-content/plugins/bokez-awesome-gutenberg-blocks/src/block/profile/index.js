//  Import CSS .
import './style/style.scss';
import './style/editor.scss';

//  Import JS .
import { Edit } from './edit';
import { ProfileIcon } from '../../icons';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;

registerBlockType( 'bokez/profile', {
    
    title: __( 'Profile' ),
    
    description: __('Add beautiful customized profile.'),
    
    keywords: [__('profile'), __('author')],
    
    icon: ProfileIcon,
    
    category: 'bokez',
    
    attributes: {
        message: {
            type: 'string',
            default: 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.',
        },
        name: {
            type: 'string',
            default: 'John Doe',
        },
        job: {
            type: 'string',
            default: 'Designer',
        },
        imageUrl: {
            type: 'string',
        },
        imageID: {
            type: 'number',
        },
        backgroundColor: {
            type: 'string',
            default: 'makeMeIgnored()',
        },
        textColor: {
            type: 'string',
            default: 'makeMeIgnored()',
        },
    },
    
    edit: Edit,
    
    save: ( props ) => {

        const { className } = props;
    
        const { 
            message,
            name,
            job,
            imageID,
            imageUrl,
            backgroundColor,
            textColor,
        } = props.attributes
    
        const style = {
            'background-color': backgroundColor,
            'color': textColor,		
        };
    
        return (
    
            <div key={ 'editable' } className={ 'bokez-profile' } style={ style } >
    
                { imageUrl && (
                    <div className = { 'bokez-profile-avatar-wrapper' } >
    
                        <div className = { 'bokez-profile-avatar' } >
                            <img src = { imageUrl } />
                        </div>
    
                    </div>
                )}
    
                <div className = { 'bokez-profile-content' } >
                
                    <RichText.Content
                        key = { 'editable-name' }
                        className = { 'bokez-profile-name' }
                        tagName = { 'h3' }
                        value = { name }
                        style = {{ 'color' : textColor }}
                    />
    
                    <RichText.Content 
                        key = { 'editable-job' }
                        className = { 'bokez-profile-job' }
                        tagName = { 'span' }
                        value = { job }
                    />
    
                    <RichText.Content 
                        key = { 'editable-text' }
                        className = { 'bokez-profile-text' }
                        tagName = { 'p' }
                        value = { message }
                    />
    
                </div>
    
            </div>
    
        );
    },
})

