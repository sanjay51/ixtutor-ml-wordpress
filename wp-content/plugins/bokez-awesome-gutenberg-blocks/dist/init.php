<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load Blocks
require_once __DIR__ . '/blocks/posts-grid/index.php';

/**
 * Enqueue Gutenberg block assets for both frontend and backend.
 *
 * `wp-blocks`: includes block type registration and related functions.
 * 
 * @since 1.0.0
 */
function bokez_gutenberg_cgb_block_assets() {

	// Styles.
	wp_enqueue_style(
		'bokez_gutenberg-cgb-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-blocks' ), // Dependency to include the CSS after it.
		BOKEZ_VERSION
	);

	if( ! is_admin() ){

		// Scripts.
		wp_enqueue_script(
			'bokez_gutenberg-cgb-frontend-js', // Handle.
			plugins_url( '/dist/frontend.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
			array( 'jquery' ), // Dependencies, defined above.
			BOKEZ_VERSION,
			true // Enqueue the script in the footer.
		);

	}

}
add_action( 'enqueue_block_assets', 'bokez_gutenberg_cgb_block_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * `wp-blocks`: includes block type registration and related functions.
 * `wp-element`: includes the WordPress Element abstraction for describing the structure of your blocks.
 * `wp-i18n`: To internationalize the block's text.
 *
 * @since 1.0.0
 */
function bokez_gutenberg_cgb_editor_assets() {

	// Scripts.
	wp_enqueue_script(
		'bokez_gutenberg-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element' ), // Dependencies, defined above.
		BOKEZ_VERSION,		
		true // Enqueue the script in the footer.
	);

	// Styles.
	wp_enqueue_style(
		'bokez_gutenberg-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
		BOKEZ_VERSION
	);

}
add_action( 'enqueue_block_editor_assets', 'bokez_gutenberg_cgb_editor_assets' );

/**
 * Register A Block Categories
 * @since 1.5.0
 */
function bokez_gutenberg_block_categories( $categories, $post ) {
	return array_merge(
        $categories,
        array(
            array(
                'slug' => 'bokez',
                'title' => 'Bokez',
            ),
        )
    );
}
add_filter( 'block_categories', 'bokez_gutenberg_block_categories', 10, 2 );
