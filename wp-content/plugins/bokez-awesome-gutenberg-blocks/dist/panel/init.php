<?php 

/**
 * Admin Page
 */
function bokez_admin_menu() {
	add_menu_page( 'Bokez', 'Bokez Blocks', 'manage_options', 'bokez_admin_page', 'bokez_admin_page', 'dashicons-layout', 200  );
}
add_action( 'admin_menu', 'bokez_admin_menu' );

/**
 * Render the admin page
 */
function bokez_admin_page(){
    require_once __DIR__ . '/view.php';
}

/**
 * Assets
 */
function bokez_admin_page_assets( $key ) {
    
    if( $key !== 'toplevel_page_bokez_admin_page' ) return;

    wp_enqueue_style( 'bokez-admin-page', BOKEZ_URL . 'dist/panel/style.css', array(), BOKEZ_VERSION );

    wp_enqueue_script( 'bokez-admin-page', BOKEZ_URL . 'dist/panel/script.js', array('jquery'), BOKEZ_VERSION, true );

}
add_action('admin_enqueue_scripts', 'bokez_admin_page_assets');
